<?php 

// Implementation of preprocess_html(). 
// ==================================================================================
function echo_preprocess_html(&$variables) {
    if (!empty($variables['page']['mini_sidebar'])) {
        $variables['classes_array'][] = 'has-mini-sidebar';
    }

    if (!empty($variables['page']['sidebar'])) {
        $variables['classes_array'][] = 'has-sidebar';
    }
}




// Implementation of preprocess_page(). 
// ==================================================================================
function echo_preprocess_page(&$vars) {
    $vars['primary_local_tasks'] = menu_primary_local_tasks();
    $vars['secondary_local_tasks'] = menu_secondary_local_tasks();
}



// Implementation of preprocess_node(). 
// ==================================================================================
function echo_preprocess_node(&$vars) {
	if ($plugin = context_get_plugin('reaction', 'block')) { 
		$vars['mini_sidebar'] = $plugin->block_get_blocks_by_region('mini_sidebar'); 
	}
}