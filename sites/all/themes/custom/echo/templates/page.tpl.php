<header>
	<div class="container header">
		<div class="row">
			<div class="branding col-md-4 col-xs-9">
				<a href="/" id="home-link" title="BSCENE Magazine">
					<span>BSCENE Magazine</span>
				</a>
			</div> <!-- /.branding -->
			
			<div id="primary-nav" class="col-md-8 col-xs-3">
				<a href="#mmenu_right" class="mobile-menu"><i class="glyphicon glyphicon-menu-hamburger"></i></a>
				<?php print render($page['primary_nav']); ?>
			</div> <!-- /#primary-nav -->
		</div>
	</div> <!-- /.container.header -->
</header> <!-- /header -->


<section>
	<div class="container section">
		
		<?php if ($page['pre_content']): ?>
		<div class="row">
			<?php if ($show_messages && $messages): ?><div class="admin-alerts"><?=$messages ?></div><?php endif; ?>

            <?php if ($primary_local_tasks): ?><ul class='primary-tabs clearfix'><?php print render($primary_local_tasks) ?></ul><?php endif; ?>
            <?php if ($secondary_local_tasks): ?><ul class='secondary-tabs clearfix'><?php print render($secondary_local_tasks) ?></ul><?php endif; ?>
			
			<div class="preContent col-xs-12">
				<?php print render($page['pre_content']); ?>
			</div> <!-- /.preContent -->
		</div>
		<?php endif; ?>
		
		<div class="row">
			<div class="mainContent col-sm-8 col-xs-12">
				<?php if($title && !$node->nid): ?>
					<h1 class="page-title"><?=$title ?></h1>
				<?php endif; ?>
				
				<?php if($section_title): ?>
					<h1 class="page-title"><?=$section_title?></h1>
				<?php endif; ?>
				
				<?php print render($page['content']); ?>
			</div> <!-- /.mainContent -->
			
			<?php if ($page['sidebar']): ?>
			<div class="sidebarContent col-sm-4 col-xs-12">
				<?php print render($page['sidebar']);?>
			</div> <!-- /.sidebarContent -->
			<?php endif; ?>
		</div>

		<?php if ($page['post_content']): ?>
		<div class="row">
			<div class="posrContent col-xs-12">
				<?php print render($page['post_content']); ?>
			</div> <!-- /.postContent -->
		</div>
		<?php endif; ?>
	</div> <!-- /.container.section -->
</section> <!-- /section -->


<footer>
	<div class="footer-top">
		<div class="container footer">
			<div class="col-md-8 menu">
				<ul>
					<li class="label label-default"><a href="/home">Home | Design</a></li>
					<li class="label label-default"><a href="/health">Health | Fitness</a></li>
					<li class="label label-default"><a href="/eat">Eat | Drink</a></li>
					<li class="label label-default"><a href="/beauty">Style | Beauty</a></li>
					<li class="label label-default"><a href="/man-about-town">Man About Town</a></li>
					<li class="label label-default"><a href="/style-file">Style File</a></li>
					<li class="label label-default"><a href="/calendar">Events</a></li>
				</ul>
			</div>
			
			<div class="col-md-4 contact">
				<a href="/"><span>BSCENE Magazine</span></a>
				<p><i class="fa fa-map-marker"></i> <?=variable_get('street_address', '')?>, <?=variable_get('city', '')?>, <?=variable_get('state', '')?> <?=variable_get('zip', '')?></p>
				<p><i class="fa fa-phone"></i> <?=variable_get('telephone', '')?> | <i class="fa fa-fax"></i> <?=variable_get('fax', '')?></p>
			</div>
		</div> <!-- /.container.footer -->
	</div>

	<div class="footer-bottom">	
		<div class="container">
			<div class="col-sm-6 social">
				<?php include 'social.tpl.php'; ?>
			</div>
			
			<div class="col-sm-6 copyright">
				<p>&copy; <?=date('Y');?> <a href="http://www.bscenemag.com">BSCENE Magazine</a> | All Rights Reserved. | <a href="/user/login">Admin Login</a></p>
			</div>
		</div> <!-- /.container.footer -->
	</div>
</footer> <!-- /footer -->


<script type="text/javascript">
   $(document).ready(function() {
      $("#mmenu_right").mmenu({
         // options
      }, {
         // configuration
         offCanvas: {
            pageNodetype: "section"
         }
      });
   });
</script>