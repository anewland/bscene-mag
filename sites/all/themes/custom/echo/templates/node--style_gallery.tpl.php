<div class="node style-gallery <?=($teaser ? 'teaser' : 'full-node');?>">

	<?php if ($teaser): ?>
	
		<div class="title"><a href="/node/<?=$node->nid?>"><?=$title?></a></div>
		<div class="style-galleries"><?=render($content['field_gallery_photos']['0']); ?></div> <!-- /.style-galleries -->
		
	<?php else: ?>
		<h1 class="page-title <?php if(!$node->field_image){ print 'no-media'; }?>"><?=$title?></h1>
		<div class="row taxonomy">
			<div class="col-xs-6 section">STYLE FILE</div>
			<div class="col-xs-6 issue"><?=render($content['field_issue_year']); ?> <?=render($content['field_issue_month']); ?></div>
		</div>
		
		<div class="style-galleries"><?=render($content['field_gallery_photos']); ?></div> <!-- /.style-galleries -->

		<div class="social-sharing">
			<!-- BEGIN ADDTHIS SOCIAL SHARE -->
				<div class="addthis_sharing_toolbox"></div>
			<!-- END ADDTHIS SOCIAL SHARE -->
		</div> <!-- /.social-sharing -->

		<div class="node-content">
			<?=render($content['body']); ?>
		</div> <!-- /.node-content -->
	
		<div class="commenting">
			<h2>Comments</h2>
			<div id="fb-root"></div>			
			<div class="fb-comments" data-href="http://www.bscenemag.com/node/<?=$node->nid?>" data-width="100%" data-numposts="10"></div>
		</div> <!-- /.commenting -->
	<?php endif; ?>

</div> <!-- /.node -->