<?php
if($_SERVER['HTTP_HOST'] == 'bscenemag.local') {
	$servername = "localhost";
	$username = "root";
	$password = "password";
} else {
	$servername = "localhost";
	$username = "root";
	$password = "bscene*h3";
}
	

// Create connection
$conn = new mysqli($servername, $username, $password);

// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
} 
echo "Connected successfully";


$query = "SELECT node.nid AS nid, node.title AS node_title, location.latitude AS location_latitude, location.longitude AS location_longitude, location.street AS location_street, location.city AS location_city, field_data_field_phone.field_phone_value AS phone_value FROM node node LEFT JOIN location_instance location_instance ON node.vid = location_instance.vid LEFT JOIN location location ON location_instance.lid = location.lid LEFT JOIN field_data_field_phone field_data_field_phone ON node.nid = field_data_field_phone.entity_id WHERE (node.status <> 0) AND (node.type in ('distro_location')) ORDER BY node_title ASC";

$result = $conn->query($query);


// Send the headers
header('Content-type: text/xml');

echo '<markers>';

if ($result->num_rows > 0) {

	//$chars = array("&");
	//$title =  str_replace($chars, "&amp;", $row['node_title']);
	//$title = "TITLE";
	$title = htmlentities($row['node_title'], ENT_QUOTES, "UTF-8");
	
	$lat = $row['location_latitude'];
	$lng = $row['location_longitude'];
	$html = '&lt;span&gt;&lt;b&gt;'.$title.'&lt;/b&gt;&lt;br/&gt; '.$row['location_street'].', '.$row['location_city'].' &lt;br/&gt; '.$row['phone_value'].' &lt;br/&gt; &lt;a href="http://'.$row['website_value'].'" target="_blank"&gt;'.$row['website_value'].'&lt;/a&gt;&lt;span&gt;';
	$label = '&lt;b&gt;'.$title.'&lt;/b&gt;, '.$row['location_street'].', '.$row['location_city'];
	
	echo "<marker lat='".$lat."' lng='".$lng."' html='".$html."' label='".$label."' />";
	
}

echo '</markers>';

$conn->close();	
?>