<!-- iOS -->
<?php if (variable_get('ios', '')) : ?>
	<a target="_blank" class="social-link ios" href="<?=variable_get('ios', '')?>" title="BSCENE on the Apple App Store">
		<span class="fa fa-apple"></span>
	</a>
<?php endif; ?>

<!-- ANDROID -->
<?php if (variable_get('android', '')) : ?>
	<a target="_blank" class="social-link android" href="<?=variable_get('android', '')?>" title="BSCENE on Google Play">
		<span class="fa fa-android"></span>
	</a>
<?php endif; ?>

<!-- FACEBOOK -->
<?php if (variable_get('facebook', '')) : ?>
	<a target="_blank" class="social-link facebook" href="<?=variable_get('facebook', '')?>" title="BSCENE on Facebook">
		<span class="fa fa-facebook"></span>
	</a>
<?php endif; ?>

<!-- TWITTER -->
<?php if (variable_get('twitter', '')) : ?>
	<a target="_blank" class="social-link twitter" href="<?=variable_get('twitter', '')?>" title="BSCENE on Twitter">
		<span class="fa fa-twitter"></span>
	</a>
<?php endif; ?>

<!-- INSTAGRAM -->
<?php if (variable_get('instagram', '')) : ?>
	<a target="_blank" class="social-link instagram" href="<?=variable_get('instagram', '')?>" title="BSCENE on Instagram">
		<span class="fa fa-instagram"></span>
	</a>
<?php endif; ?>

<!-- PINTEREST -->
<?php if (variable_get('pinterest', '')) : ?>
	<a target="_blank" class="social-link pinterest" href="<?=variable_get('pinterest', '')?>" title="BSCENE on Pinterest">
		<span class="fa fa-pinterest"></span>
	</a>
<?php endif; ?>

<!-- YOUTUBE -->
<?php if (variable_get('youtube', '')) : ?>
	<a target="_blank" class="social-link youtube" href="<?=variable_get('youtube', '')?>" title="BSCENE on YouTube">
		<span class="fa fa-youtube"></span>
	</a>
<?php endif; ?>

<!-- GOOGLE+ -->
<?php if (variable_get('google', '')) : ?>
	<a target="_blank" class="social-link google" href="<?=variable_get('google', '')?>" title="BSCENE on Google+">
		<span class="fa fa-google-plus"></span>
	</a>
<?php endif; ?>

<!-- LINKEDIN -->
<?php if (variable_get('linkedin', '')) : ?>
	<a target="_blank" class="social-link linkedin" href="<?=variable_get('linkedin', '')?>" title="BSCENE on Linkedin">
		<span class="fa fa-linkedin"></span>
	</a>
<?php endif; ?>

<!-- RSS FEEDS -->
<?php if (variable_get('rss', '')) : ?>
	<a target="_blank" class="social-link rss" href="/rss" title="BSCENE Magazine RSS Feeds">
		<span class="fa fa-rss"></span>
	</a>
<?php endif; ?>

