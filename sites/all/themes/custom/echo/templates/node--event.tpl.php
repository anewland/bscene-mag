<div class="node event <?=($teaser ? 'teaser' : 'full-node');?>">

<?php 
	$dst = date('I');
	$is_dst = ($dst == '1') ? 5 : 6;
	$eventDate = strtotime($node->field_event_date['und']['0']['value'].'- '.$is_dst.' hours'); 
?>

<?php if ($teaser): ?>
	
	<div <?php if($node->field_cover_unveil == '1') { print 'class="cover-unveil"'; }?>>
		<a href="/node/<?=$node->nid?>">
			
			<div class="info">
				<p class="title"><?=$title?></p>
				<div class="date"><?=date('M', $eventDate);?> <?=date('d', $eventDate);?> @ <?=date('h:i a', $eventDate);?> | <?=$node->field_location['und']['0']['name'];?> <span>|</span>  <?=$node->field_location['und']['0']['city'];?>, <?=$node->field_location['und']['0']['province'];?></div>
				<div class="descript">					
					<?php if ($node->field_image): ?>
						<div class="event-picture"><?=render($content['field_image']);?></div>
					<?php endif; ?>
					<?=render($content['body']);?>
					
					<p class="readmore"><a href="/node/<?=$node->nid?>">Continue Reading <i class="glyphicon glyphicon-circle-arrow-right"></i></a></p>

				</div>
			</div>
		</a>
	</div>

<?php else: ?>

		<h1 class="page-title"><?=$title?> <span><?=render($content['field_issue_month']); ?> <?=render($content['field_issue_year']); ?></span></h1>
	
		<div class="media"><?=render($content['field_image']); ?></div> <!-- /.media -->
		
		<div class="social-sharing">
			<!-- BEGIN ADDTHIS SOCIAL SHARE -->
				<div class="addthis_sharing_toolbox"></div>
			<!-- END ADDTHIS SOCIAL SHARE -->
		</div> <!-- /.social-sharing -->
		
		<div class="node-content">
			<div class="event-information">
			
				<h4>Date &amp; Time</h4>
				<p class="field"><?=date('l, F d, Y @ h:i a', $eventDate);?></p>
				
				<h4>Location</h4>
				<?=render($content['field_location']);?>
				
				<h4>Details</h4>
				<?=render($content['body']);?>
				
				<div class="map" style="display:none;">
					<?=gmap_simple_map($node->field_location['und']['0']['latitude'], $node->field_location['und']['0']['longitude'], '', render($content['field_location']), 14, '495px', '380px', true)?>
				</div> <!-- /.map -->
			</div> <!-- /.event-information -->
				
			<div id="mini-sidebar">				
				<?php print render($mini_sidebar); ?>
			</div> <!-- /#mini-sidebar -->
			
		</div> <!-- /.node-content -->
			
		<div class="social-sharing">
			<!-- BEGIN ADDTHIS SOCIAL SHARE -->
				<div class="addthis_sharing_toolbox"></div>
			<!-- END ADDTHIS SOCIAL SHARE -->
		</div> <!-- /.social-sharing -->
	
		<div class="commenting">
			<h2>Comments</h2>
			<div id="fb-root"></div>			
			<div class="fb-comments" data-href="http://www.bscenemag.com/node/<?=$node->nid?>" data-width="100%" data-numposts="10"></div>
		</div> <!-- /.commenting -->

<?php endif; ?>

</div>