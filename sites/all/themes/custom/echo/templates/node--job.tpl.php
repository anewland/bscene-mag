<div class="node <?=$id;?> <?=($teaser ? 'teaser' : 'full-node');?>">
<?php if ($teaser): ?>

		<?php if($node->field_image): ?>
			<div class="media"><?=render($content['field_image']); ?></div> <!-- /.media -->
		<?php endif; ?>			
	
		<div class="teaser-content <?php if(!$node->field_image){ print 'no-media'; }?>">
			<h3><a href="/node/<?=$node->nid?>"><?=$title?></a></h3>
		
			<?=render($content['body']); ?>
			<p class="readmore"><a href="/node/<?=$node->nid?>">Continue Reading <i class="glyphicon glyphicon-circle-arrow-right"></i></a></p>
			
		</div> <!-- /.teaser-content -->
		
		<?php if($node->field_image): ?>
			<div class="taxonomy">
				<?=render($content['field_issue_month']); ?> <?=render($content['field_issue_year']); ?>
			</div> <!-- /.taxonomy -->
		<?php endif; ?>

<?php else: ?>
	<h1 class="page-title no-media"><?=$title?> <span><?=render($content['field_section']);?></span></h1>
	
	<div class="social-sharing">
		<!-- BEGIN ADDTHIS SOCIAL SHARE -->
			<div class="addthis_sharing_toolbox"></div>
		<!-- END ADDTHIS SOCIAL SHARE -->
	</div> <!-- /.social-sharing -->
	
	<div class="node-content">
		<?=render($content['body']); ?>
	</div> <!-- /.node-content -->
	
	<div class="attachments">
		<?=render($content['field_attachments']); ?>
	</div> <!-- /.attachments -->
<?php endif; ?>
</div>