<div class="node <?=$id;?> <?=($teaser ? 'teaser' : 'full-node');?>">

	<?php if ($teaser): ?>

		<?php if($node->field_image): ?>
			<div class="media"><?=render($content['field_image']); ?></div> <!-- /.media -->
		<?php endif; ?>			
	
		<div class="teaser-content <?php if(!$node->field_image){ print 'no-media'; }?>">
			<h3><a href="/node/<?=$node->nid?>"><?=$title?></a></h3>
			<h4 class="taxonomy">
				<?=render($content['field_issue_month']); ?> <?=render($content['field_issue_year']); ?>
			</h4> <!-- /.taxonomy -->
		
			<?=render($content['body']); ?>
			<p class="readmore"><a href="/node/<?=$node->nid?>">Continue Reading <i class="glyphicon glyphicon-circle-arrow-right"></i></a></p>
			
		</div> <!-- /.teaser-content -->
	
	<?php else: ?>
		<h1 class="page-title <?php if(!$node->field_image){ print 'no-media'; }?>"><?=$title?></h1>
		<div class="row taxonomy">
			<div class="col-xs-6 section"><?=render($content['field_section']);?></div>
			<div class="col-xs-6 issue"><?=render($content['field_issue_year']); ?> <?=render($content['field_issue_month']); ?></div>
		</div>
	
		<?php if($node->field_image): ?>
			<div class="media"><?=render($content['field_image']); ?></div> <!-- /.media -->
		<?php endif; ?>
		
		<div class="social-sharing">
			<!-- BEGIN ADDTHIS SOCIAL SHARE -->
				<div class="addthis_sharing_toolbox"></div>
			<!-- END ADDTHIS SOCIAL SHARE -->
		</div> <!-- /.social-sharing -->
		
		<div class="node-content">
			<?php if($node->field_image): ?>
				<div class="byline"><?=render($content['field_byline']); ?></div>
			<?php endif; ?>
			<?=render($content['body']); ?>
		</div> <!-- /.node-content -->
		
		<div class="social-sharing">
			<!-- BEGIN ADDTHIS SOCIAL SHARE -->
				<div class="addthis_sharing_toolbox"></div>
			<!-- END ADDTHIS SOCIAL SHARE -->
		</div> <!-- /.social-sharing -->
	
		<div class="commenting">
			<h2>Comments</h2>
			<div id="fb-root"></div>			
			<div class="fb-comments" data-href="http://www.bscenemag.com/node/<?=$node->nid?>" data-width="100%" data-numposts="10"></div>
		</div> <!-- /.commenting -->
	<?php endif; ?>

</div> <!-- /.node -->