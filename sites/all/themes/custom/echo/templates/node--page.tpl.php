<div class="full-node">
	
	<h1 class="page-title no-media"><?=$title?> <span><?=render($content['field_section']);?></span></h1>
	
	<div class="social-sharing">
		<!-- BEGIN ADDTHIS SOCIAL SHARE -->
			<div class="addthis_sharing_toolbox"></div>
		<!-- END ADDTHIS SOCIAL SHARE -->
	</div> <!-- /.social-sharing -->
	
	<div class="node-content">
		<?=render($content['body']); ?>
	</div> <!-- /.node-content -->
	
	<div class="attachments">
		<?=render($content['field_attachments']); ?>
	</div> <!-- /.attachments -->

</div>