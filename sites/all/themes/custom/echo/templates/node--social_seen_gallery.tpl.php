<div class="node <?=$id;?> <?=($teaser ? 'teaser' : 'full-node');?>">

	<?php if ($teaser): ?>
	
		<div class="media">
			<a href="<?=strip_tags(render($content['field_gallery_link'])); ?>" target="_blank">
				<div class="bscene-branding"></div>
				<?=render($content['field_bs_gallery_photo']); ?>
				<span><?=$title?></span>
			</a>
		</div> <!-- /.media -->
		
	<?php else: ?>
	
		<h1 class="page-title"><?=$title?></h1>
	
		<div class="media"><?=render($content['field_bs_gallery_photo']); ?></div> <!-- /.media -->
		
		<div class="social-sharing">
			<!-- BEGIN ADDTHIS SOCIAL SHARE -->
				<div class="addthis_sharing_toolbox"></div>
			<!-- END ADDTHIS SOCIAL SHARE -->
		</div> <!-- /.social-sharing -->
	
		<div class="commenting">
			<h2>Comments</h2>
			<div id="fb-root"></div>			
			<div class="fb-comments" data-href="http://www.bscenemag.com/node/<?=$node->nid?>" data-width="100%" data-numposts="10"></div>
		</div> <!-- /.commenting -->
	<?php endif; ?>

</div>