<?php $videoLink = $node->field_youtube_link['und']['0']['value']; ?>

<?php if ($teaser): ?>

	<div class="teaser-content <?php if(!$node->field_image){ print 'no-media'; }?>">
		<h3><a href="/node/<?=$node->nid?>"><?=$title?></a></h3>
	
		<?=render($content['body']); ?>
		<p class="readmore"><a href="/node/<?=$node->nid?>"><span>Read Full Article &raquo;</span></a></p>
		
	</div> <!-- /.teaser-content -->

	<?php if($node->field_image): ?>
		<div class="media"><?=render($content['field_image']); ?></div> <!-- /.media -->
	<?php endif; ?>			
	
<?php else: ?>

	<div class="btv-video">
		<?php
			$path = str_replace('http://www.youtube.com/watch?v=', 'http://www.youtube.com/embed/', $videoLink);
			$path = array_shift(explode('&', $path));
			print '<iframe title="'.$title.'" width="632" height="356" src="'.$path.'" frameborder="0" allowfullscreen></iframe>';
		?>
	</div> <!-- /.btv-video -->

	<div class="commenting">
		<h2>Comments</h2>
		<?=render($content['facebook_comments']); ?>
	</div> <!-- /.commenting -->
<?php endif; ?>