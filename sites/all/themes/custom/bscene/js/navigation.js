$(document).ready(function() {
	$('#header > .menu > li').mouseover(function() {
		$(this).children('li > a').addClass('hover');
		$(this).children('.menu').show();
	});	
	
	$('#header > .menu > li').mouseout(function() {
		$(this).children('li > a').removeClass('hover');
		$(this).children('.menu').hide();
	});	
});