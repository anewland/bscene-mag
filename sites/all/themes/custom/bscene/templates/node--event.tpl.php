<?php $eventDate = strtotime($node->field_event_date['und']['0']['value'].'- 6 hours'); ?>

<?php if ($teaser): ?>
	
	<div <?php if($node->field_cover_unveil == '1') { print 'class="cover-unveil"'; }?>>
		<a href="/node/<?=$node->nid?>">
			<div class="date"><?=date('M', $eventDate);?> <span><?=date('d', $eventDate);?> </span></div>
			<div class="info">
				<p class="title"><?=$title?></p>
				<p class="location"><?=date('h:i a', $eventDate);?> <span>|</span> <?=$node->field_location['und']['0']['name'];?> <em> <span>|</span>  <?=$node->field_location['und']['0']['city'];?>, <?=$node->field_location['und']['0']['province'];?></em>
				</p>
			</div>
			<?php if ($node->field_image): ?>
				<div class="event-picture"><?=render($content['field_image']);?></div>
			<?php endif; ?>
		</a>
	</div>

<?php else: ?>

		<h1 class="page-title"><?=$title?> <span><?=render($content['field_issue_month']); ?> <?=render($content['field_issue_year']); ?></span></h1>
	
		<div class="media"><?=render($content['field_image']); ?></div> <!-- /.media -->
		
		<div class="social-sharing">
			<div class="fb"><script src="http://connect.facebook.net/en_US/all.js#xfbml=1"></script><fb:like href="<?=$_SERVER["SERVER_NAME"].$_SERVER["REQUEST_URI"]?>" layout="button_count" show_faces="false" width="160"></fb:like></div>
			
			<div class="tweet"><a href="http://twitter.com/share" class="twitter-share-button" data-count="horizontal">Tweet</a><script type="text/javascript" src="http://platform.twitter.com/widgets.js"></script></div>
			
			<div class="gplus"><script type="text/javascript" src="https://apis.google.com/js/plusone.js"></script><g:plusone size="medium" href="<?=$_SERVER["SERVER_NAME"].$_SERVER["REQUEST_URI"]?>"></g:plusone></div>
			
		</div> <!-- /.social-sharing -->
		
		<div class="node-content">
			<div class="event-information">
			
				<h4>Date &amp; Time</h4>
				<p class="field"><?=date('l, F d, Y @ h:i a', $eventDate);?></p>
				
				<h4>Location</h4>
				<?=render($content['field_location']);?>
				
				<h4>Details</h4>
				<?=render($content['body']);?>
				
				<div class="map" style="display:none;">
					<?=gmap_simple_map($node->field_location['und']['0']['latitude'], $node->field_location['und']['0']['longitude'], '', render($content['field_location']), 14, '495px', '380px', true)?>
				</div> <!-- /.map -->
			</div> <!-- /.event-information -->
				
			<div id="mini-sidebar">				
				<?php print render($mini_sidebar); ?>
			</div> <!-- /#mini-sidebar -->
			
		</div> <!-- /.node-content -->
			
		<div class="commenting">
			<h2>Comments</h2>
			<?=render($content['facebook_comments']); ?>
		</div> <!-- /.commenting -->

<?php endif; ?>
