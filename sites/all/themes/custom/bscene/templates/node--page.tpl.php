<h1 class="page-title no-media"><?=$title?> <span><?=render($content['field_section']);?></span></h1>

<div class="social-sharing">
	<div class="fb"><script src="http://connect.facebook.net/en_US/all.js#xfbml=1"></script><fb:like href="<?=$_SERVER["SERVER_NAME"].$_SERVER["REQUEST_URI"]?>" layout="button_count" show_faces="false" width="160"></fb:like></div>
	
	<div class="tweet"><a href="http://twitter.com/share" class="twitter-share-button" data-count="horizontal">Tweet</a><script type="text/javascript" src="http://platform.twitter.com/widgets.js"></script></div>
	
	<div class="gplus"><script type="text/javascript" src="https://apis.google.com/js/plusone.js"></script><g:plusone size="medium" href="<?=$_SERVER["SERVER_NAME"].$_SERVER["REQUEST_URI"]?>"></g:plusone></div>
	
</div> <!-- /.social-sharing -->

<div class="node-content">
	<?=render($content['body']); ?>
</div> <!-- /.node-content -->

<div class="attachments">
	<?=render($content['field_attachments']); ?>
</div> <!-- /.attachments -->
