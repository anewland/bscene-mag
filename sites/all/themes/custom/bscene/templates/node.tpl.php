<div class="node <?=$id?>">

	<?php if ($teaser): ?>

		<?php if($node->field_image): ?>
			<div class="media"><?=render($content['field_image']); ?></div> <!-- /.media -->
		<?php endif; ?>			
	
		<div class="teaser-content <?php if(!$node->field_image){ print 'no-media'; }?>">
			<h3><a href="/node/<?=$node->nid?>"><?=$title?></a></h3>
			<h4 class="taxonomy">
				<?=render($content['field_issue_month']); ?> <?=render($content['field_issue_year']); ?>
			</h4> <!-- /.taxonomy -->
		
			<?=render($content['body']); ?>
			<p class="readmore"><a href="/node/<?=$node->nid?>">Read Full Article &raquo;</a></p>
			
		</div> <!-- /.teaser-content -->
	
	<?php else: ?>
		<h1 class="page-title <?php if(!$node->field_image){ print 'no-media'; }?>"><?=$title?> <span><?=render($content['field_section']);?></span></h1>
	
		<?php if($node->field_image): ?>
			<div class="media"><?=render($content['field_image']); ?></div> <!-- /.media -->
		<?php endif; ?>
		
		<div class="social-sharing">
			<div class="fb"><script src="http://connect.facebook.net/en_US/all.js#xfbml=1"></script><fb:like href="<?=$_SERVER["SERVER_NAME"].$_SERVER["REQUEST_URI"]?>" layout="button_count" show_faces="false" width="160"></fb:like></div>
			
			<div class="tweet"><a href="http://twitter.com/share" class="twitter-share-button" data-count="horizontal">Tweet</a><script type="text/javascript" src="http://platform.twitter.com/widgets.js"></script></div>
			
			<div class="gplus"><script type="text/javascript" src="https://apis.google.com/js/plusone.js"></script><g:plusone size="medium" href="<?=$_SERVER["SERVER_NAME"].$_SERVER["REQUEST_URI"]?>"></g:plusone></div>
			
		</div> <!-- /.social-sharing -->
		
		<div class="node-content">
			<?=render($content['body']); ?>
	
			<div id="mini-sidebar">
				<div class="byline">
					<?php if($node->field_byline): ?>
						<p class="author"><?=render($content['field_byline']); ?></p>
					<?php else: ?>
						<?php
						  $node_author = user_load($node->uid);
				          print '<p class="author">'.$node_author->field_full_name['und']['0']['value'];
				          print '<span class="title">'.$node_author->field_title['und']['0']['value'].'</span></p>';
						?>
					<?php endif; ?>
					<div class="month"><?=render($content['field_issue_month']); ?> <?=render($content['field_issue_year']); ?></div>
				</div> <!-- /.byline -->
				
				<?php print render($mini_sidebar); ?>
			</div> <!-- /#mini-sidebar -->
		</div> <!-- /.node-content -->
	
		<div class="commenting">
			<h2>Comments</h2>
			<?=render($content['facebook_comments']); ?>
		</div> <!-- /.commenting -->
	<?php endif; ?>

</div> <!-- /.node -->