<div class="node style-gallery">

	<?php if ($teaser): ?>
	
		<div class="title"><a href="/node/<?=$node->nid?>"><?=$title?></a></div>
		<div class="style-galleries"><?=render($content['field_gallery_photos']['0']); ?></div> <!-- /.style-galleries -->
		
	<?php else: ?>
		<h1 class="page-title <?php if(!$node->field_image){ print 'no-media'; }?>"><?=$title?> <span><?=render($content['field_issue_month']); ?> <?=render($content['field_issue_year']); ?> Style File</span></h1>
		
		<div class="style-galleries"><?=render($content['field_gallery_photos']); ?></div> <!-- /.style-galleries -->

		<div class="social-sharing">
			<div class="fb"><script src="http://connect.facebook.net/en_US/all.js#xfbml=1"></script><fb:like href="<?=$_SERVER["SERVER_NAME"].$_SERVER["REQUEST_URI"]?>" layout="button_count" show_faces="false" width="160"></fb:like></div>
			
			<div class="tweet"><a href="http://twitter.com/share" class="twitter-share-button" data-count="horizontal">Tweet</a><script type="text/javascript" src="http://platform.twitter.com/widgets.js"></script></div>
			
			<div class="gplus"><script type="text/javascript" src="https://apis.google.com/js/plusone.js"></script><g:plusone size="medium" href="<?=$_SERVER["SERVER_NAME"].$_SERVER["REQUEST_URI"]?>"></g:plusone></div>
			
		</div> <!-- /.social-sharing -->

		<div class="node-content">
			<?=render($content['body']); ?>
	
			<div id="mini-sidebar">
				<?php print render($mini_sidebar); ?>
			</div> <!-- /#mini-sidebar -->
		</div> <!-- /.node-content -->
		
	
		<div class="commenting">
			<h2>Comments</h2>
			<?=render($content['facebook_comments']); ?>
		</div> <!-- /.commenting -->
	<?php endif; ?>

</div> <!-- /.node -->