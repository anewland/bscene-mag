<div id="header">
	<div class="container">

		<div class="branding">
			<a href="/" id="home-link" title="BSCENE Magazine | The Magazine of East Texas"><span>Home</span></a>
		</div> <!== /.branding -->
		
		<div class="social">
			<ul>
				<?php if (variable_get('apps', '')) : ?>
					<li class="last mobile"><a href="/apps" title="Mobile"><span>Download our Mobile Apps</span></a></li>
				<?php endif; ?>
				<?php if (variable_get('rss', '')) : ?>
					<li class="rss"><a href="/feeds" title="RSS Feeds"><span>Subscribe to our RSS feeds</span></a></li>
				<?php endif; ?>
				<?php if (variable_get('google', '')) : ?>
				 	<li class="google"><a href="<?=variable_get('google', '')?>" title="BSCENE Mag on Twitter"><span>BSCENE Mag on Google+</span></a></li>
				<?php endif; ?>
				<?php if (variable_get('twitter', '')) : ?>
				 	<li class="twitter"><a href="<?=variable_get('twitter', '')?>" title="BSCENE Mag on Twitter"><span>BSCENE Mag on Twitter</span></a></li>
				<?php endif; ?>				
				<?php if (variable_get('facebook', '')) : ?>
				 	<li class="first facebook"><a href="<?=variable_get('facebook', '')?>" title="BSCENE Mag on Facebook"><span>BSCENE Mag on Facebook</span></a></li>
				<?php endif; ?>				
			</ul>
		</div> <!== /.social -->

		<div id="primary-nav"><?php print render($page['primary_nav']); ?></div> <!-- /#primary-nav -->
		
	</div> <!-- /.container -->
</div> <!-- /#header -->


<div id="section">
	<div class="container">
		
		<div id="content-container">
            <?php if ($show_messages && $messages): ?><div class="admin-alerts"><?=$messages ?></div><?php endif; ?>

            <?php if ($primary_local_tasks): ?><ul class='primary-tabs clearfix'><?php print render($primary_local_tasks) ?></ul><?php endif; ?>
            <?php if ($secondary_local_tasks): ?><ul class='secondary-tabs clearfix'><?php print render($secondary_local_tasks) ?></ul><?php endif; ?>
            
            <?php if ($page['pre_content']): ?>
            	<div id="pre-content"><?php print render($page['pre_content']); ?></div> <!-- /#pre-content -->
            <?php endif; ?>

            <div id="copy">
			
				<?php if($title && !$node->nid): ?>
					<h1 class="page-title"><?=$title ?></h1>
				<?php endif; ?>
				
				<?php if($section_title): ?>
					<h1 class="page-title"><?=$section_title?></h1>
				<?php endif; ?>
			
				<div id="main-content"><?php print render($page['content']); ?></div> <!-- /#main-content -->
            </div> <!-- /#copy -->
            
            <?php if ($is_front && $page['mini_sidebar']): ?>
            	<div id="mini-sidebar"><?php print render($page['mini_sidebar']); ?></div> <!-- /#mini-sidebar -->
            <?php endif; ?>
            
            <?php if ($page['post_content']): ?>
            	<div id="post-content"><?php print render($page['post_content']); ?></div> <!-- /#post-content -->
            <?php endif; ?>
                        
		</div> <!-- /#content-container -->

        <?php if ($page['sidebar']): ?>
            <div id="sidebar"><?=render($page['sidebar']);?></div> <!-- /#sidebar -->
        <?php endif; ?>		
		
		<div class="clear"></div>
	</div> <!-- /.container -->
</div> <!-- /#section -->


<div id="footer">
	<div class="container">
		<?php print(render(menu_tree('menu-footer-menu'))); ?>
		
		<p class="copyright">
			<span>Copyright &copy; <?=date('Y')?> <strong><?=variable_get('copyright_notice', '')?></strong> | All Rights Reserved.</span>
			<span><?php if (variable_get('telephone', '')) : ?>
					PHONE: <?=variable_get('telephone', '')?> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
				  <?php endif; ?>
				  <?php if (variable_get('fax', '')) : ?>
					FAX: <?=variable_get('fax', '')?> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
				  <?php endif; ?>
				  <?php if (variable_get('site_mail', '')) : ?>
					EMAIL: <a href="mailto:<?=variable_get('site_mail', '')?>"><?=variable_get('site_mail', '')?></a>
				  <?php endif; ?></span>
		</p>
		<p class="address">
			<span><?=variable_get('street_address', '')?></span>
			<span><?=variable_get('city', '')?>, <?=variable_get('state', '')?> <?=variable_get('zip', '')?></span>
		</p>
	</div> <!-- /.container -->
</div> <!-- /#footer -->